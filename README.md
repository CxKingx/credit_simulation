**BCA Technical Assessment**

**By: Kevin Ferdinand**

[**https://gitlab.com/CxKingx/credit_simulation/**](https://gitlab.com/CxKingx/credit_simulation/)
# **Overview**
This is a Console Application to calculate Loan payments yearly based on user information through JSON file or user input, the app will automatically reject any inputs that does not obey the rule restriction like loan amount, vehicle type and others. 

- The project is coded in java using the MVC (Model View Controller) System. In IntelliJ 
- JUNIT Tests are included, some functions are not tested because I am confused with the Scanner JUNIT Test as it is not working for Mock Scanner. 
- Gitlab Is used for repository and CI/CD
- The CI/CD is not fully working as it’s a new subject for me and I have not experienced it before
- The main file is in credit\_simulator.java.
# **Folder Architecture**
The main files in their directories. For the Main Classes and the Junit Test.

![A screenshot of a computer Description automatically generated](images/1.png) 
![A screenshot of a computer program Description automatically generated](images/Aspose.Words.a62aed40-6862-419b-aa09-63615d9610d3.002.png)



# **Running the File**

## IntelliJ
By Opening the project and run the credit\_simulator.java we can run the project.

![A screenshot of a computer Description automatically generated](images/Aspose.Words.a62aed40-6862-419b-aa09-63615d9610d3.028.png)

If we use the Edit Configuration for the credit\_simulator.java , we can also put the input file as argument so the program will read it.

![A screenshot of a computer Description automatically generated](images/Aspose.Words.a62aed40-6862-419b-aa09-63615d9610d3.029.png)
## Jar File
java -jar credit\_simulator.jar

java -jar credit\_simulator.jar file\_inputs.json

These 2 commands can be used to run the file from the Command Window.

![image](images/jar_run1.png)
![image](images/jar_run2.png)


