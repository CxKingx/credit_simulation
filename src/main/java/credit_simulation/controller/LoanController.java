package credit_simulation.controller; // Adjust the package name as needed

import credit_simulation.model.LoanModel;
import credit_simulation.model.VehicleModel;
import credit_simulation.view.LoanView;
import com.fasterxml.jackson.databind.ObjectMapper; // Import Jackson ObjectMapper
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.Scanner;

public class LoanController {
    private LoanModel model;
    private LoanView view;
    private Scanner scanner = new Scanner(System.in);
    DecimalFormat df = new DecimalFormat("#0.00");
    DecimalFormat dfthousands = new DecimalFormat("#,###.00");
    public LoanController(LoanModel model, LoanView view) {
        this.model = model;
        this.view = view;
    }

    public void initialize(){
        view.init();

    }

    // Get UserInput for Option 1
    public boolean getUserInput(LoanModel model) {
        VehicleModel vehicleModel = new VehicleModel();

        // User Inputs
        System.out.print("Enter vehicle condition (Baru/Bekas): ");
        String vehicleCondition = scanner.nextLine();
        if (!isValidVehicleCondition(vehicleCondition)) {
            System.out.println("Invalid input for vehicle condition.");
            return false;
        }
        vehicleModel.setVehicleCondition(vehicleCondition);

        System.out.print("Enter vehicle type (Mobil/Motor): ");
        String vehicleType = scanner.nextLine();
        if (!isValidVehicleType(vehicleType)) {
            System.out.println("Invalid input for vehicle type.");
            return false;
        }
        vehicleModel.setVehicleType(vehicleType);

        System.out.print("Enter vehicle year (4-digit): ");
        String tahunMobil = scanner.nextLine();
        if (!isValidVehicleYear(tahunMobil,vehicleCondition)) {
            System.out.println("Invalid input for vehicle year.");
            return false;
        }
        vehicleModel.setTahunMobil(tahunMobil);

        System.out.print("Enter Tenor Cicilan (Years): ");
        String tenorCicilan = scanner.nextLine();
        if (!isValidTenorCicilan(tenorCicilan)) {
            System.out.println("Invalid input for Tenor Cicilan (Years).");
            return false;
        }
        vehicleModel.setTenorCicilan(tenorCicilan);

        System.out.print("Enter Loan Amount / Jumlah Pinjaman (Rp): ");
        String loanAmount = scanner.nextLine();
        if (!isValidLoanAmount(loanAmount)) {
            System.out.println("Invalid input for Jumlah Pinjaman.");
            return false;
        }
        vehicleModel.setJumlahPinjaman(loanAmount);
        showMinimalDownPayment(vehicleModel);

        System.out.print("Enter Down Payment (Rp) : ");
        String downPayment = scanner.nextLine();
        if (!isValidDownPayment(downPayment , vehicleModel)) {
            System.out.println("Invalid input for Down Payment / Minimum not met.");
            return false;
        }
        vehicleModel.setJumlahDownPayment(downPayment);

        // Set the vehicleModel in the LoanModel
        model.setVehicleModel(vehicleModel);

        // Prompt for responseCode and responseMessage (if needed)
        System.out.print("Enter response code: ");
        String responseCode = scanner.nextLine();
        model.setResponseCode(responseCode);

        System.out.print("Enter response message: ");
        String responseMessage = scanner.nextLine();
        model.setResponseMessage(responseMessage);

        return true;
    }
    // When User Puts its Jumlah Pinjaman, show the minimal Down payment for it
    // If user still put less than minimal, will give error or don't want to put

    private void showMinimalDownPayment(VehicleModel vehicleModel){
        try {
            int loanAmount = Integer.parseInt(vehicleModel.getJumlahPinjaman());
            if (vehicleModel.getVehicleCondition().equalsIgnoreCase("baru")) {
                // For Mobil, down payment must be >= 35% of the loan amount
                int minimumDownPayment = (int) (0.35 * loanAmount);
                System.out.println("Minimum Down Payment is " + minimumDownPayment);

            } else if (vehicleModel.getVehicleCondition().equalsIgnoreCase("bekas")) {
                // For Motor, down payment must be >= 25% of the loan amount
                int minimumDownPayment = (int) (0.25 * loanAmount);
                System.out.println("Minimum Down Payment is " + minimumDownPayment);
            }

        } catch (NumberFormatException ignored) {
        }
    }
    // If json file is put in the args
    public boolean processFileInput(String fileName) {
        // Implement logic to process file input
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            LoanModel loadedModel = objectMapper.readValue(new File(fileName), LoanModel.class);

            // Check if the loaded data is within the specified ranges
            if (!isValidVehicleType(loadedModel.getVehicleModel().getVehicleType())) {
                System.out.println("Invalid vehicle type in the JSON file.");
                return false;
            }

            if (!isValidVehicleCondition(loadedModel.getVehicleModel().getVehicleCondition())) {
                System.out.println("Invalid vehicle condition in the JSON file.");
                return false;
            }

            if (!isValidVehicleYear( loadedModel.getVehicleModel().getTahunMobil() , loadedModel.getVehicleModel().getVehicleCondition())){
                System.out.println("Invalid vehicle year in the JSON file.");
                return false;
            }

            if (!isValidLoanAmount(loadedModel.getVehicleModel().getJumlahPinjaman())) {
                System.out.println("Invalid loan amount in the JSON file.");
                return false;
            }

            if (!isValidDownPayment(loadedModel.getVehicleModel().getJumlahDownPayment(), loadedModel.getVehicleModel())) {
                System.out.println("Invalid down payment in the JSON file.");
                return false;
            }

            if (!isValidTenorCicilan(loadedModel.getVehicleModel().getTenorCicilan())) {
                System.out.println("Invalid loan duration in the JSON file.");
                return false;
            }

            // Update the current model with the loaded data
            model.setVehicleModel(loadedModel.getVehicleModel());
            model.setResponseCode(loadedModel.getResponseCode());
            model.setResponseMessage(loadedModel.getResponseMessage());
        } catch (IOException e) {
            System.err.println("Error loading JSON file: " + e.getMessage());
        }
        return false;
    }

    public boolean isValidVehicleCondition(String condition) {
        return condition.equalsIgnoreCase("baru") || condition.equalsIgnoreCase("bekas");
    }

    public boolean isValidVehicleType(String type) {
        return type.equalsIgnoreCase("Mobil") || type.equalsIgnoreCase("Motor");
    }

    public boolean isValidVehicleYear(String year , String condition) {
        // If year is 4digits
        if (!year.matches("\\d{4}")) {
            return false;
        }
        int yearValue = Integer.parseInt(year);
        int currentYear = LocalDate.now().getYear();

        // Check if the year is positive and not greater than the current year
        if (yearValue <= 0 || yearValue > currentYear) {
            return false;
        }

        // If the vehicle condition is "Baru," check that the year is either the current year or the previous year
        if (condition.equalsIgnoreCase("Baru")) {
            int previousYear = currentYear - 1;
            return yearValue == currentYear || yearValue == previousYear;
        }

        // For other vehicle conditions, any year is considered valid
        return true;


    }
    // Loan max is 1billion / 1 miliyar and no negative
    public boolean isValidLoanAmount(String loanAmount) {
        try {
            int amount = Integer.parseInt(loanAmount);
            return amount >= 0 && amount <= 1000000000;
        } catch (NumberFormatException e) {
            return false;
        }
    }
    // Duration of loan 1-6 year
    public boolean isValidTenorCicilan(String cicilan) {
        try {
            int amount = Integer.parseInt(cicilan);
            return amount >= 1 && amount <= 6;
        } catch (NumberFormatException e) {
            return false;
        }
    }
    // Check if it corresponds to the minimum down payment
    public boolean isValidDownPayment(String payment, VehicleModel vehicleModel) {
        try {
            int amount = Integer.parseInt(payment);
            //System.out.println(amount);
            int loanAmount = Integer.parseInt(vehicleModel.getJumlahPinjaman());
            if (vehicleModel.getVehicleCondition().equalsIgnoreCase("baru")) {
                // For Mobil, down payment must be >= 35% of the loan amount
                int minimumDownPayment = (int) (0.35 * loanAmount);
                //System.out.println(minimumDownPayment);

                return (amount >= minimumDownPayment) && (loanAmount > amount);
            } else if (vehicleModel.getVehicleCondition().equalsIgnoreCase("bekas")) {
                // For Motor, down payment must be >= 25% of the loan amount
                int minimumDownPayment = (int) (0.25 * loanAmount);
                //System.out.println(minimumDownPayment);
                return (amount >= minimumDownPayment) && (loanAmount > amount);
            } else {
                // Invalid vehicle type
                return false;
            }
        } catch (NumberFormatException e) {
            return false;
        }
    }

    // Calculate Loan based on Formula
    public void calculateLoan(LoanModel model) {
        System.out.println("Calculating Loan");
        VehicleModel vehicleModel = model.getVehicleModel();
        double baseInterestRate;
        double yearlyInterestRate ;
        double AverageMonthlyPayment;
        double TotalPayment = 0;
        int loanDuration = Integer.parseInt(vehicleModel.getTenorCicilan());
        double loanAmount = Double.parseDouble(vehicleModel.getJumlahPinjaman());
        double downPayment = Double.parseDouble(vehicleModel.getJumlahDownPayment());
        double remainingLoan = loanAmount - downPayment;
        List<Double> yearlyInterestRateList = new ArrayList<>();

         //Determine the base interest rate based on vehicle type
        if (vehicleModel.getVehicleType().equalsIgnoreCase("Mobil")) {
            baseInterestRate = 0.08; // Base interest rate for Mobil
        } else if (vehicleModel.getVehicleType().equalsIgnoreCase("Motor")) {
            baseInterestRate = 0.09; // Base interest rate for Motor
        } else {
            System.out.println("Invalid vehicle type.");
            return;
        }
        yearlyInterestRate = baseInterestRate;


        for (int year = 1; year <= loanDuration; year++) {
            int TenorRemaining = loanDuration - year+1;
            if (year == 2 || year == 4 || year == 6) {
                yearlyInterestRate = yearlyInterestRate + 0.001; // Increase by 0.1 for Year 2, 4 and Year 6
            } else if (year == 3 || year == 5) {
                yearlyInterestRate = yearlyInterestRate + 0.005; // Increase by 0.5 for Year 3 and Year 5
            } else {
                yearlyInterestRate = baseInterestRate; // Base interest rate for Year 1
            }
            double loanTotalThisYearGrowth = remainingLoan + (remainingLoan * yearlyInterestRate);
            double YearlyInstallment = loanTotalThisYearGrowth / TenorRemaining;
            yearlyInterestRateList.add(YearlyInstallment);
            TotalPayment = TotalPayment + YearlyInstallment;
            remainingLoan = loanTotalThisYearGrowth - YearlyInstallment;
            double MonthlyInstallment = YearlyInstallment / 12;

            System.out.println("Tahun " + year + ": Rp." + dfthousands.format(MonthlyInstallment) + "/month , Suku Bunga = "+ df.format(yearlyInterestRate * 100)+"%");

        }
        AverageMonthlyPayment = TotalPayment / loanDuration / 12;
        System.out.println("Total Payment Rp." + dfthousands.format(TotalPayment));
        System.out.println("Average Monthly Payment Rp." + dfthousands.format(AverageMonthlyPayment)+"\n");


    }

}
