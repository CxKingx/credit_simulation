package credit_simulation; // Adjust the package name as needed

import credit_simulation.model.LoanModel;
import credit_simulation.view.LoanView;
import credit_simulation.controller.LoanController;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

// Template from https://github.com/abdullahsumbal/mvc_template Python,
// https://www.tutorialspoint.com/design_pattern/mvc_pattern.htm Java
// Using Java

public class credit_simulator {
    // Args for receiving file name
    public static void main(String[] args) {
        // Create instances of the model, view, and controller
        LoanModel model = new LoanModel();
        LoanView view = new LoanView();
        LoanController controller = new LoanController(model, view);
        List<LoanModel> loanModelList = new ArrayList<>();
        // Initialize the controller
        controller.initialize();

        if (args.length > 0 && args[0].endsWith(".json")) {
            // Process file input if a file argument is provided (e.g., ./credit_simulator file_inputs.txt)
            System.out.println("File Argument Found");
            controller.processFileInput(args[0]);
            // Display Loan Detail and Calculate the Loan
            view.displayLoanDetails(model);
            controller.calculateLoan(model);

            // Create a new Object for the Model and the calculated Yearly Installment
            loanModelList.add(model);
            menuLoop(controller, view , loanModelList);

        } else {
            // No file argument provided, so ask the user for input
            menuLoop(controller, view , loanModelList);
            view.close();
        }

    }

    public static void menuLoop(LoanController controller, LoanView view
            , List<LoanModel> loanModelList) {

        view.showMenu();
        Scanner scanner = new Scanner(System.in);
        label:
        while (true) {
            System.out.print("Enter your choice: ");
            String choice = scanner.nextLine().trim();

            switch (choice) {
                case "1":
                    // The user chose to calculate a loan, so get user input
                    LoanModel userModel = new LoanModel();
                    boolean validInput = controller.getUserInput(userModel);

                    if (validInput) {
                        view.displayLoanDetails(userModel);
                        controller.calculateLoan(userModel);
                        loanModelList.add(userModel);
                    }

                    break;
                case "2":
                    System.out.println("There are " + loanModelList.size() + " uploaded loans, which are ");
                    // Use the View to display this
                    view.displayLoanDetailsWithCalculatedLoan(loanModelList, controller);

                    break;
                case "3":
                    // The user chose to exit the program
                    break label;
                default:
                    System.out.println("Invalid choice. Please choose 1 or 2.");
                    break;
            }

            // Display the menu again after each operation
            view.showMenu();
        }

        scanner.close();
        view.close();
    }
}
