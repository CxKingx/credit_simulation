package credit_simulation.model; // Adjust the package name as needed

public class VehicleModel {
    // Default Value is Null
    private String vehicleCondition;
    private String vehicleType;
    private String tahunMobil;
    private String jumlahDownPayment;
    private String jumlahPinjaman;
    private String tenorCicilan;

    // Put the Get and Set , I have read Lombok - Maven can help simplify but Not Experienced for it so Manual use

    public String getVehicleCondition() {
        return vehicleCondition;
    }

    public void setVehicleCondition(String vehicleCondition) {
        this.vehicleCondition = vehicleCondition;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getTahunMobil() {
        return tahunMobil;
    }

    public void setTahunMobil(String tahunMobil) {
        this.tahunMobil = tahunMobil;
    }

    public String getJumlahDownPayment() {
        return jumlahDownPayment;
    }

    public void setJumlahDownPayment(String jumlahDownPayment) {
        this.jumlahDownPayment = jumlahDownPayment;
    }

    public String getJumlahPinjaman() {
        return jumlahPinjaman;
    }

    public void setJumlahPinjaman(String jumlahPinjaman) {
        this.jumlahPinjaman = jumlahPinjaman;
    }

    public String getTenorCicilan() {
        return tenorCicilan;
    }

    public void setTenorCicilan(String tenorCicilan) {
        this.tenorCicilan = tenorCicilan;
    }
}
