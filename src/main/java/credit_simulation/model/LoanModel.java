package credit_simulation.model; // Adjust the package name as needed

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
// Start from the Model, the File will get Loan Amount, Interest Rate and Loan Duration in Months

public class LoanModel {
    private VehicleModel vehicleModel;
    private String responseCode;
    private String responseMessage;

    public VehicleModel getVehicleModel() {
        return vehicleModel;
    }

    public void setVehicleModel(VehicleModel vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }


}
