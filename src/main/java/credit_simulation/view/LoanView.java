package credit_simulation.view; // Adjust the package name as needed

import java.util.List;
import java.util.Scanner;

import credit_simulation.controller.LoanController;
import credit_simulation.model.LoanModel;
import credit_simulation.model.VehicleModel;

public class LoanView {
    private Scanner scanner;
    public LoanView() {
        scanner = new Scanner(System.in);
    }

    public void init() {
        // Initialize the user interface if needed
    }

    public void showMenu() {
        // Display the initial menu or user interface to the user
        System.out.println("Welcome to the Loan Calculator!");
        System.out.println("Choose an option:");
        System.out.println("1. Calculate Loan");
        System.out.println("2. Show Calculated Loans");
        System.out.println("3. Exit");
    }

    public void displayLoanDetails(LoanModel model) {
        // Retrieve input values from the model and print them
        VehicleModel vehicleModel = model.getVehicleModel();

        System.out.println("Vehicle Condition: " + vehicleModel.getVehicleCondition());
        System.out.println("Vehicle Type: " + vehicleModel.getVehicleType());
        System.out.println("Vehicle Year: " + vehicleModel.getTahunMobil());
        System.out.println("Tenor Cicilan (Years): " + vehicleModel.getTenorCicilan());
        System.out.println("Loan Amount / Jumlah Pinjaman (Rp): " + vehicleModel.getJumlahPinjaman());
        System.out.println("Down Payment (Rp): " + vehicleModel.getJumlahDownPayment());
    }

    public void displayLoanDetailsWithCalculatedLoan(List<LoanModel> yearlyLoanModelList, LoanController controller){
        if (yearlyLoanModelList.isEmpty()) {
            System.out.println("There are no uploaded Loan information");
        }
        else{
            // Calculate the loans again from the Model
            for (int index = 0 ; index < yearlyLoanModelList.size(); index++){
                System.out.println("\nLoan No."+(index+1));
                displayLoanDetails(yearlyLoanModelList.get(index));
                controller.calculateLoan(yearlyLoanModelList.get(index));
            }
        }

    }

    public void close() {
        // Close any resources (e.g., scanner) if needed
        scanner.close();
    }

    // For JUNIT Test
    public void setScanner(Scanner mockScanner) {
        this.scanner = mockScanner;
    }


    public Scanner getScanner() {
        return scanner;
    }
}
