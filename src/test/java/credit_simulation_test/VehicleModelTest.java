package credit_simulation_test; // Adjust the package name

import credit_simulation.model.VehicleModel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

// https://www.javatpoint.com/junit-test-case-example-in-java
// https://junit.org/junit4/javadoc/4.13/org/junit/Assert.html

public class VehicleModelTest {
    private VehicleModel vehicle;

    @BeforeEach
    void setUp() {
        vehicle = new VehicleModel();
    }

    @Test
    void testDefaultValues() {
        // Ensure that the default values are set correctly
        assertNull(vehicle.getVehicleType());
        assertNull(vehicle.getVehicleCondition());
        assertNull(vehicle.getTahunMobil());
        assertNull(vehicle.getJumlahDownPayment());
        assertNull(vehicle.getJumlahPinjaman());
        assertNull(vehicle.getTenorCicilan());
    }

    // Try setting the values, probably later the ignore case is in front before passing backend
    @Test
    void testSetAndGetMethods() {
        // Test setting and getting values for various attributes
        vehicle.setVehicleType("Mobil");
        vehicle.setVehicleCondition("Baru");
        vehicle.setTahunMobil("2019");
        vehicle.setJumlahDownPayment("25000000");
        vehicle.setJumlahPinjaman("100000000");
        vehicle.setTenorCicilan("5");

        assertEquals("Mobil", vehicle.getVehicleType());
        assertEquals("Baru", vehicle.getVehicleCondition());
        assertEquals("2019", vehicle.getTahunMobil());
        assertEquals("25000000", vehicle.getJumlahDownPayment());
        assertEquals("100000000", vehicle.getJumlahPinjaman());
        assertEquals("5", vehicle.getTenorCicilan());
    }

    //Testing for Setting as Null
    @Test
    void testNullValues() {

        vehicle.setVehicleType(null);
        vehicle.setVehicleCondition(null);

        assertNull(vehicle.getVehicleType());
        assertNull(vehicle.getVehicleCondition());
    }
}
