package credit_simulation_test; // Adjust the package name

import com.fasterxml.jackson.databind.ObjectMapper;
import credit_simulation.controller.LoanController;
import credit_simulation.model.LoanModel;
import credit_simulation.model.VehicleModel;
import credit_simulation.view.LoanView;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

public class LoanModelTest {
    private LoanModel loanModel;

    @BeforeEach
    void setUp() {
        loanModel = new LoanModel();
    }

    @Test
    void testDefaultValues() {
        // Ensure that the default values are set correctly
        assertNull(loanModel.getVehicleModel());
        assertNull(loanModel.getResponseCode());
        assertNull(loanModel.getResponseMessage());
    }

    @Test
    void testSetAndGetVehicleModel() {
        // Test setting and getting the vehicle model
        VehicleModel vehicleModel = new VehicleModel();
        vehicleModel.setVehicleType("Car");
        vehicleModel.setVehicleCondition("New");

        loanModel.setVehicleModel(vehicleModel);

        assertEquals(vehicleModel, loanModel.getVehicleModel());
    }

    @Test
    void testSetAndGetResponseCode() {
        // Test setting and getting the response code
        loanModel.setResponseCode("00");

        assertEquals("00", loanModel.getResponseCode());
    }

    @Test
    void testSetAndGetResponseMessage() {
        // Test setting and getting the response message
        loanModel.setResponseMessage("Succeed");

        assertEquals("Succeed", loanModel.getResponseMessage());
    }

    @Test
    public void testLoadFromJson() {
        LoanModel loanModel = new LoanModel();
        LoanView view = new LoanView();
        LoanController controller = new LoanController(loanModel, view);
        controller.processFileInput("file_inputs3.json");

        // Assert the expected values against the actual values loaded from the JSON file
        assertEquals("Bekas", loanModel.getVehicleModel().getVehicleCondition());
        assertEquals("Mobil", loanModel.getVehicleModel().getVehicleType());
        assertEquals("2019", loanModel.getVehicleModel().getTahunMobil());
        assertEquals("25000000", loanModel.getVehicleModel().getJumlahDownPayment());
        assertEquals("100000000", loanModel.getVehicleModel().getJumlahPinjaman());
        assertEquals("3", loanModel.getVehicleModel().getTenorCicilan());
        assertEquals("00", loanModel.getResponseCode());
        assertEquals("Succeed", loanModel.getResponseMessage());
    }

}
