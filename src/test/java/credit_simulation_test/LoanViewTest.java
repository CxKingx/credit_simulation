package credit_simulation_test;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.After;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;
import credit_simulation.model.LoanModel;
import credit_simulation.model.VehicleModel;
import credit_simulation.view.LoanView;
import credit_simulation.controller.LoanController;

public class LoanViewTest {
    private LoanModel model;
    private LoanView view;
    private LoanController controller;
    private ByteArrayInputStream mockInput;
    private ByteArrayOutputStream mockOutput;

    @Before
    public void setUp() {
        model = new LoanModel();
        view = new LoanView();
        controller = new LoanController(model, view);

        // Redirect System.in and System.out for testing user input and output
        mockOutput = new ByteArrayOutputStream();
        System.setOut(new PrintStream(mockOutput));
    }

    @After
    public void tearDown() {
        // Restore the original System.in and System.out
        System.setIn(System.in);
        System.setOut(System.out);
    }

    @Test
    public void testDisplayLoanDetails() {
        VehicleModel vehicleModel = new VehicleModel();
        vehicleModel.setVehicleCondition("baru");
        vehicleModel.setVehicleType("Mobil");
        vehicleModel.setTahunMobil("2022");
        vehicleModel.setTenorCicilan("5");
        vehicleModel.setJumlahPinjaman("1000000000");
        vehicleModel.setJumlahDownPayment("350000000");

        model.setVehicleModel(vehicleModel);

        view.displayLoanDetails(model);
        String output = mockOutput.toString();

        assertTrue(output.contains("Vehicle Condition: baru"));
        assertTrue(output.contains("Vehicle Type: Mobil"));
        assertTrue(output.contains("Vehicle Year: 2022"));
        assertTrue(output.contains("Tenor Cicilan (Years): 5"));
        assertTrue(output.contains("Loan Amount / Jumlah Pinjaman (Rp): 1000000000"));
        assertTrue(output.contains("Down Payment (Rp): 350000000"));
    }

    @Test
    public void testDisplayLoanDetailsWithCalculatedLoan() {
        VehicleModel vehicleModel = new VehicleModel();
        vehicleModel.setVehicleCondition("baru");
        vehicleModel.setVehicleType("Mobil");
        vehicleModel.setTahunMobil("2022");
        vehicleModel.setTenorCicilan("5");
        vehicleModel.setJumlahPinjaman("1000000000");
        vehicleModel.setJumlahDownPayment("350000000");

        model.setVehicleModel(vehicleModel);

        List<LoanModel> yearlyLoanModelList = new ArrayList<>();
        yearlyLoanModelList.add(model);

        view.displayLoanDetailsWithCalculatedLoan(yearlyLoanModelList, controller);
        String output = mockOutput.toString();

        assertTrue(output.contains("Loan No.1"));
        assertTrue(output.contains("Vehicle Condition: baru"));
        assertTrue(output.contains("Vehicle Type: Mobil"));
        assertTrue(output.contains("Vehicle Year: 2022"));
        assertTrue(output.contains("Tenor Cicilan (Years): 5"));
        assertTrue(output.contains("Loan Amount / Jumlah Pinjaman (Rp): 1000000000"));
        assertTrue(output.contains("Down Payment (Rp): 350000000"));
        assertTrue(output.contains("Calculating Loan"));
    }

    @Test
    public void testShowMenu() {
        view.showMenu();
        String output = mockOutput.toString();

        assertTrue(output.contains("Welcome to the Loan Calculator!"));
        assertTrue(output.contains("Choose an option:"));
        assertTrue(output.contains("1. Calculate Loan"));
        assertTrue(output.contains("2. Show Calculated Loans"));
        assertTrue(output.contains("3. Exit"));
    }

    @Test
    public void testSetScanner() {
        ByteArrayInputStream mockInput = new ByteArrayInputStream("test input".getBytes());
        Scanner mockScanner = new Scanner(mockInput);

        view.setScanner(mockScanner);
        assertEquals(mockScanner, view.getScanner());
    }

    // Add more test methods as needed for other functionalities
}
