package credit_simulation_test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import credit_simulation.controller.LoanController;
import credit_simulation.model.LoanModel;
import credit_simulation.model.VehicleModel;
import credit_simulation.view.LoanView;
import org.mockito.internal.configuration.injection.scanner.MockScanner;

public class LoanControllerTest {
    private LoanModel model;
    private MockScanner mockScanner;
    private LoanView view;
    private LoanController controller;
    @Before
    public void setUp() {
        model = new LoanModel();
        //mockScanner = new MockScanner();

        controller = new LoanController(model, null); // You can pass null for the view in the test
    }
    @Test
    public void testInvalidVehicleType() {
        LoanModel model = new LoanModel();
        LoanView view = new LoanView();
        LoanController controller = new LoanController(model, view);

        // Simulate loading a JSON file with an invalid vehicle type
        String json = "{\"vehicleModel\":{\"vehicleCondition\":\"Baru\",\"vehicleType\":\"Bicycle\",\"tahunMobil\":\"2022\",\"jumlahDownPayment\":\"25000000\",\"jumlahPinjaman\":\"100000000\",\"tenorCicilan\":\"5\"},\"responseCode\":\"00\",\"responseMessage\":\"Succeed\"}";

        assertFalse(controller.processFileInput(json)); // Expect false because of invalid vehicle type

    }

    @Test
    public void testInvalidVehicleYear() {
        LoanModel model = new LoanModel();
        LoanView view = new LoanView();
        LoanController controller = new LoanController(model, view);

        // Simulate loading a JSON file with an invalid vehicle year
        String json = "{\"vehicleModel\":{\"vehicleCondition\":\"Baru\",\"vehicleType\":\"Mobil\",\"tahunMobil\":\"2025\",\"jumlahDownPayment\":\"25000000\",\"jumlahPinjaman\":\"100000000\",\"tenorCicilan\":\"5\"},\"responseCode\":\"00\",\"responseMessage\":\"Succeed\"}";

        assertFalse(controller.processFileInput(json)); // Expect false because of invalid vehicle year

    }

    @Test
    public void testInvalidDownPayment() {
        LoanModel model = new LoanModel();
        LoanView view = new LoanView();
        LoanController controller = new LoanController(model, view);

        // Simulate loading a JSON file with an invalid down payment
        String json = "{\"vehicleModel\":{\"vehicleCondition\":\"Baru\",\"vehicleType\":\"Mobil\",\"tahunMobil\":\"2022\",\"jumlahDownPayment\":\"10000\",\"jumlahPinjaman\":\"100000000\",\"tenorCicilan\":\"5\"},\"responseCode\":\"00\",\"responseMessage\":\"Succeed\"}";

        assertFalse(controller.processFileInput(json)); // Expect false because of invalid down payment

    }

    @Test
    public void testInvalidLoanAmount() {
        LoanModel model = new LoanModel();
        LoanView view = new LoanView();
        LoanController controller = new LoanController(model, view);

        // Simulate loading a JSON file with an invalid loan amount
        String json = "{\"vehicleModel\":{\"vehicleCondition\":\"Baru\",\"vehicleType\":\"Mobil\",\"tahunMobil\":\"2022\",\"jumlahDownPayment\":\"25000000\",\"jumlahPinjaman\":\"2000000000\",\"tenorCicilan\":\"5\"},\"responseCode\":\"00\",\"responseMessage\":\"Succeed\"}";

        assertFalse(controller.processFileInput(json));
    }
    @Test
    public void testinvalidJSON1() {
        LoanModel model = new LoanModel();
        LoanView view = new LoanView();
        LoanController controller = new LoanController(model, view);

        // Simulate loading a JSON file with an invalid loan amount
        String json = "file_inputs.json";
        controller.processFileInput(json);
        assertFalse(controller.processFileInput(json));
        String json2 = "file_inputs2.json";
        controller.processFileInput(json2);
        assertFalse(controller.processFileInput(json2));

    }
}
